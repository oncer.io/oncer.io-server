import { Inject, Injectable, Logger } from '@nestjs/common';
import { Database } from 'arangojs';
import { DocumentCollection } from 'arangojs/collection';

@Injectable()
export class PaypalService {
	private readonly logger = new Logger(PaypalService.name);
	private paypalSubscriptionsCollection: DocumentCollection;
	private paypalEventsCollection: DocumentCollection;

	constructor(
		@Inject(Database)
		private db: Database
	) {
		this.paypalSubscriptionsCollection = this.db.collection(
			'paypalSubscriptions'
		);
		this.paypalEventsCollection = this.db.collection('paypalEvents');
		const paypalSubscriptionsCollection = this.paypalSubscriptionsCollection;
		const paypalEventsCollection = this.paypalEventsCollection;
		async function initialiseDb() {
			const paypalSubscriptionsCollectionExists =
				await paypalSubscriptionsCollection.exists();
			if (!paypalSubscriptionsCollectionExists) {
				await paypalSubscriptionsCollection.create();
			}
			const paypalEventsCollectionExists =
				await paypalEventsCollection.exists();
			if (!paypalEventsCollectionExists) {
				await paypalEventsCollection.create();
			}
		}
		initialiseDb();
	}

	async recordPayPalEvent(event: any) {
		try {
			await this.paypalEventsCollection.save(event);
		} catch (err) {
			Logger.error(err);
		}
	}

	async createOrUpdateSubscription(paypalSubscription: {
		paypalSubscriptionResourceId: string;
		appSubscriptionId?: string;
		paypalPlanId?: string;
		status?: string;
		validUntil?: number;
	}) {
		const {
			appSubscriptionId: appSubscriptionId,
			paypalSubscriptionResourceId,
			paypalPlanId,
			validUntil,
			status,
		} = paypalSubscription;
		const exists = await this.paypalSubscriptionsCollection.documentExists(
			paypalSubscriptionResourceId
		);
		if (exists) {
			const current = await this.paypalSubscriptionsCollection.document(
				paypalSubscriptionResourceId
			);
			const patch: {
				appSubscriptionId?: string;
				paypalPlanId?: string;
				validUntil?: number;
				status?: string;
			} = {};
			if (validUntil && current.validUntil !== validUntil) {
				patch.validUntil = validUntil;
			}
			if (paypalPlanId && current.paypalPlanId !== paypalPlanId) {
				patch.paypalPlanId = paypalPlanId;
			}
			if (status && current.status !== status) {
				patch.status = status;
			}
			if (
				appSubscriptionId &&
				current.appSubscriptionId !== appSubscriptionId
			) {
				patch.appSubscriptionId = appSubscriptionId;
			}
			if (!Object.keys(patch).length) {
				this.logger.log(`Empty patch skipped for ${paypalPlanId}`);
			} else {
				await this.paypalSubscriptionsCollection.update(
					paypalSubscriptionResourceId,
					patch
				);
			}
		} else {
			await this.paypalSubscriptionsCollection.save(
				Object.assign({}, paypalSubscription, {
					_key: paypalSubscriptionResourceId,
				})
			);
		}
	}
}
