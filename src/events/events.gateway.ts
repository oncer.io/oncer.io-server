import { Logger } from '@nestjs/common';
import {
	WebSocketGateway,
	WebSocketServer,
	OnGatewayConnection,
} from '@nestjs/websockets';
import { jwtDecrypt } from 'jose';
import { Server, Socket } from 'socket.io';
import { EcdhJsonWebKey } from '../types/EcdhJsonWebKey';
import { getJwtEncryptKey } from '../helpers/getJwtEncryptKey';

@WebSocketGateway(parseInt(process.env.WEBSOCKET_PORT), {
	cors: {
		origin: '*',
	},
	namespace: '/notifications',
})
export class EventsGateway implements OnGatewayConnection {
	@WebSocketServer()
	server: Server;

	async handleConnection(socket: Socket) {
		try {
			const deviceToken = socket.handshake.auth.deviceToken as string;
			if (!deviceToken) {
				Logger.warn('Missing deviceToken in ws auth');
				throw new Error('Missing deviceToken in ws auth');
			}
			const { payload } = await jwtDecrypt(deviceToken, getJwtEncryptKey());
			if (!payload || typeof payload.sub !== 'string') {
				Logger.warn('Invalid devieToken in ws auth');
				throw new Error('Invalid devieToken in ws auth');
			}
			const deviceId = payload.sub;
			socket.data.deviceId = deviceId as string;
			socket.join(deviceId);
		} catch (err) {
			Logger.error(err?.message || 'Unknown socket connection error');
			socket.conn.close();
		}
	}

	sendInviteAcceptedNotification(opts: {
		deviceId: string;
		channelId: string;
		recipientPublicKey: EcdhJsonWebKey;
	}) {
		try {
			const { deviceId, channelId, recipientPublicKey } = opts;
			this.server.to(deviceId).emit('inviteAccepted', {
				channelId,
				recipientPublicKey,
			});
		} catch (err) {
			Logger.error(err?.message || 'Unknown send invite accept error');
		}
	}

	sendMessageChangeNotification(opts: {
		deviceId: string;
		channelId: string;
		hasMessage: boolean;
		isFromSelf: boolean;
	}) {
		try {
			const { deviceId, channelId, hasMessage, isFromSelf } = opts;
			this.server.to(deviceId).emit('messageChange', {
				channelId,
				hasMessage,
				isFromSelf,
			});
		} catch (err) {
			Logger.error(err?.message || 'Unknown send message change notification');
		}
	}
}
