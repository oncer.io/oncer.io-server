Oncer.io - Message without history

Send passwords, secrets, and personal data - encrypted and without history.
